/* 
* Feito por: Gabriel Tourinho Badar�
*/

import java.io.BufferedReader;
import java.io.FileWriter; 
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class readData{
	public static void main(String[] args) {
		String path = "src/dataset.csv";
		String line = "";

		
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			FileWriter myWriter = new FileWriter("GabrielTourinhoBadaro.txt");
			br.readLine(); // Pula a primeira linha
			while((line = br.readLine()) != null) {
				
				String[] valores = line.split(";");
				
				String nome = valores[0].trim().toUpperCase().replaceAll(" +", " ") + " " + valores[1].trim().toUpperCase().replaceAll(" +", " ") + " ";
				
				
				try {
					double imcAux;
					imcAux = Double.parseDouble(valores[2].replaceAll(",", ".")) / Math.pow(Double.parseDouble(valores[3].replaceAll(",", ".")), 2);
					String imc = String.format("%,.2f", imcAux).replace(".", ",");
					myWriter.write(nome + imc + "\n");
					System.out.println(nome + imc);
				} catch (ArrayIndexOutOfBoundsException e) {
					System.out.println("Est� faltando algum valor para essa inst�ncia!");
				}
				
				
				
			}
			br.close();
			
			myWriter.close();
		} catch (FileNotFoundException e) {		
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
}